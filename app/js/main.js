function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
jQuery(document).ready(function($) {
    var datingCheck = 'volume';    
    if (getCookie(datingCheck) == undefined) {
        const check = $('.volume input[type=checkbox]').prop( "checked");        
        check ? document.cookie="volume=true" : document.cookie="volume=false";
    }
    else if (getCookie(datingCheck) == 'false') {
        $("label.volume input[type=checkbox]").prop("checked", false);
    }
    $('body').on('click', 'label.volume', function() {
        const check = $('.volume input[type=checkbox]').prop( "checked");        
        check ? document.cookie="volume=true" : document.cookie="volume=false";
        console.log(document.cookie);
    });

    $( "#switcher" ).tabs({
        active: 2
    });

    $('body').on('click', '#menu-toggle', function() {
        $('.navigation').toggleClass('active');
        $('#menu-toggle').toggleClass('is-active');
    })
    $('body').on('click', '.menu-item-has-children>a', function(e) {
        e.preventDefault();        
    })

    $(window).scroll(function() {
        inWindow($('.topsites-item'));
        inWindow($('.allsites-item'));
    });
    function inWindow(s){
        var scrollTop = $(window).scrollTop();
        var windowHeight = $(window).height();
        var currentEls = $(s);
        var result = [];
        currentEls.each(function() {
            var el = $(this);
            var offset = el.offset();
            if(scrollTop <= offset.top && (el.height() + offset.top) < (scrollTop + windowHeight) && (window.innerWidth < 767)) {
                el.addClass('hover');
            }
            else
            el.removeClass('hover');
        });
      }

    var sound  = document.getElementById('sound');
    var sound1 = document.getElementById('sound1');
    var sound2 = document.getElementById('sound2');

    var topSites = $('.topsites-item');
    topSites.mouseenter(function() {
        const check = $('.volume input[type=checkbox]').prop( "checked");
        if (check) {
            sound.play()
            // var playPromice = sound.play();
            // if (playPromice !== undefined) {
            //     playPromice.then(_ => {
            //         //  play sound
            //     }).catch(error => {
            //         console.log();
            //     });
            // }               
        }
    });
    var btnVisit = $('.btn-visit');
    if (btnVisit) {
        btnVisit.mouseenter(function() {
            const check = $('.volume input[type=checkbox]').prop( "checked");
            if (check) {   
                if($(this).parent('.fire').length) { 
                    // var playPromice2 = sound2.play()                
                    sound2.play()                
                } 
                else { 
                    // var playPromice2 = sound1.play();                
                    sound1.play();                
                }
                // if (playPromice2 !== undefined) {
                //     playPromice2.then(_ => {
                //         //  play sound
                //     }).catch(error => {
                //         console.log();
                //     });
                // }
            }
        }); 
    }
    // chose country
    var soundChose = document.getElementById('soundChose');
    var choseButton = $('.chose');
    if (choseButton) {
        choseButton.mouseenter(function() {
            const check = $('.volume input[type=checkbox]').prop( "checked");
            if (check) {
                // var playPromiceChose = soundChose.play();
                soundChose.play();
                // if (playPromiceChose !== undefined) {
                //     playPromiceChose.then(_ => {
                //         //  play sound
                //     }).catch(error => {
                //         console.log();
                //     });
                // }               
            }
        });
    } 

    // top switcher
    sizeSwitcher();
    $(window).resize(function() {
        sizeSwitcher();
    });
    function sizeSwitcher() {
        var maxWidth = [];
        const switcher = document.querySelector('.switcher-nav');
        if (switcher) {    
            const spans = document.querySelectorAll('.switcher-nav li span');
            for (var i = 0; i < spans.length; i++) {
                maxWidth[i] = spans[i].offsetWidth;
            }
            maxWidth.sort(function(a, b) {
                return b - a
            });
            const spanwidth = maxWidth[0];
            
            const widthSwitcher = switcher.offsetWidth;
            if (spanwidth > widthSwitcher/2 - 100) {
                const newWidth = 2 * (spanwidth - widthSwitcher/2) + widthSwitcher + 150;                      
                switcher.style.width = newWidth + 'px';
            } 
        }
    }
    // resize top-blogs
    resizeTopBlogs();
    $('body').on('click', '.switcher-nav li a', function() {
        resizeTopBlogs();
    })
    // timers
    // const timers = document.querySelectorAll('.clock');        
    const timers = $('.clock');        
    const currentDate = new Date();
    for (var i = 0; i < timers.length; i++) {
        // console.log(timers[i].getAttribute('data-date'));
        // const futureDate  = new Date('Oct 03 2018 13:58:19 GMT+0300');
        const futureDate  = new Date(timers[i].getAttribute('data-date'));
        var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
        const n = i + 1;
        const className = 'clock'+n;
        timers[i].classList.add(className);

        if(diff < 0) {
            diff = 0;
        }
        
        $('.'+className).FlipClock(diff, {
            clockFace: 'DailyCounter',
            countdown: true
            // language:'es-es'
        });
    }

    function dayDiff(first, second) {
        return (second-first)/(1000*60*60*24);
    }

    function resizeTopBlogs() {
        var heightItems = [];
        $('.topsites-item').each(function() {
            $(this).css('height', 'auto');
            heightItems.push($(this).outerHeight());
        })
        if ($(window).width() > 767) {
            for (var i = 0; i < 9; i = i + 3) {
                heightItems[i] >= heightItems[i+2] ? heightItems[i+2] = heightItems[i] : heightItems[i] = heightItems[i+2];
                if (heightItems[i] >= heightItems[i+1]) {
                    heightItems[i+1] = heightItems[i] + 30;
                }
                else {
                    if (heightItems[i+1] - heightItems[i+1] < 30) {
                        heightItems[i+1] = heightItems[i] + 30;
                    }
                    else {
                        
                    }
                }
            }
            // heightItems = [
            //     heightItems[6],
            //     heightItems[7],
            //     heightItems[8],
            //     heightItems[6],
            //     heightItems[7],
            //     heightItems[8],
            //     heightItems[6],
            //     heightItems[7],
            //     heightItems[8]
            // ]
            var n = 0;
            $('.topsites-item').each(function() {
                $(this).outerHeight(heightItems[n]+'px');
                n++;
            })
        }
        else {
            // heightItems.sort(function(a, b) {
            //     return a - b;
            // }) 
            $('.topsites-item').each(function() {
                $(this).css('height', 'auto');
            })
        }
    }
    $(window).resize(function() {
        resizeTopBlogs();
    })

    // rating 
    $('body').on('click', '.change li', function() {
        $(this).parent('ul').parent('.change').removeClass('change');
        // Variable for Wordpress
        var rating = $(this).attr('data-rating');
    })

    // var buttonMenu = document.getElementById('trigger-menu');
    // var fixMenu = document.getElementById('fix-nav');

    // buttonMenu.onclick = function() {
    //     fixMenu.classList.contains("active") ? fixMenu.classList.remove("active") : fixMenu.classList.add("active");
    // }
} );
